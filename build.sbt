organization := "sbt test"

name := "sbt test"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.9.1"

seq(webSettings :_*)

libraryDependencies ++= Seq(
    "org.scalatra" %% "scalatra" % "2.0.4",
    "com.fasterxml.jackson.core" %% "jackson-databind" % "2.9.7"
)
